package store;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Order {

	private Customer customer;
	private Salesman salesman;
	private Date orderedOn;
	private String deliveryStreet;
	private String deliveryCity;
	private String deliveryCountry;
	private Set<OrderItem> items;

	public Order(Customer customer, Salesman salesman, String deliveryStreet, String deliveryCity, String deliveryCountry, Date orderedOn) {
		this.customer = customer;
		this.salesman = salesman;
		this.deliveryStreet = deliveryStreet;
		this.deliveryCity = deliveryCity;
		this.deliveryCountry = deliveryCountry;
		this.orderedOn = orderedOn;
		this.items = new HashSet<OrderItem>();
	}

	public Customer getCustomer() {
		return customer;
	}

	public Salesman getSalesman() {
		return salesman;
	}

	public Date getOrderedOn() {
		return orderedOn;
	}

	public String getDeliveryStreet() {
		return deliveryStreet;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public String getDeliveryCountry() {
		return deliveryCountry;
	}

	public Set<OrderItem> getItems() {
		return items;
	}

	public float total() {
		float totalItems = calculateTotalForItems();
		float tax = calculateTax(totalItems);
		float shipping = calculateShipping();
		return totalItems + tax + shipping;	
	}

	private float calculateShipping() {
		float shipping = 0;
		if (this.deliveryCountry != "USA")
			shipping = 15;
		return shipping;
	}

	private float calculateTax(float totalItems) {
		return totalItems * 5 / 100;
	}

	private float calculateTotalForItems() {
		float totalItems = 0;
		for (OrderItem item : items) {
			float totalItem = calculatePriceWithDiscount(item);
			totalItems += totalItem;
		}
		return totalItems;
	}

	private float calculatePriceWithDiscount(OrderItem item) {

		float totalItem=0;
		float itemAmount = item.getProduct().getUnitPrice() * item.getQuantity();
		float discount = 0;
		if (item.getProduct().getCategory() == ProductCategory.Accessories) {
			if (itemAmount >= 100) {
				discount = itemAmount * 10 / 100;
			}
			totalItem = itemAmount - discount;
		}
		if (item.getProduct().getCategory() == ProductCategory.Bikes) {
			// 20% discount for Bikes
			totalItem = itemAmount - itemAmount * 20 / 100;
		}
		if (item.getProduct().getCategory() == ProductCategory.Cloathing) {
			if (item.getQuantity() > 2) {
				discount = item.getProduct().getUnitPrice();
			}
			totalItem = itemAmount - discount;
		}
		return totalItem;
	}
}
